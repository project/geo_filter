<?php
// Constant definitions for english-language text replacements
// changing these requires updating the javascript regex
define('GEO_FILTER_AT_SYMBOL', t('[at]'));
define('GEO_FILTER_DOT_SYMBOL', t('[dot]'));

/**
 * Implementation of hook_init()
 *
 * This will load the email unhiding javascript for each page
 */
function geo_filter_init() {
  drupal_add_js(drupal_get_path('module', 'geo_filter') .'/geo_filter.js');
}

/**
 * Implementation of hook_menu()
 *
 * Adds the contact module customization
 */
function geo_filter_menu() {
  $items = array();

  // menu callback for contact form builder
  $items['contact/%/%/%'] = array(
    'title' => 'Email',
    'page callback' => 'geo_filter_contact_redirect',
    'page arguments' => array(1,2,3),
    'access callback' => TRUE,
    'type' => MENU_CALLBACK,
  );

  return $items;
}

/**
 * Page callback for contact form builder
 *
 * Checks if the email address exists on the contact form, if it doesn't then it adds it. 
 * Redirects user to the contact page
 *
 * @param string $name the name portion of the email address
 * @param string $domain the domain name portion of the email address
 * @param string $tld the top-level domain of the domain name
 * @return void
 */
function geo_filter_contact_redirect($name, $domain, $tld) {
  $hidestring = $name . GEO_FILTER_AT_SYMBOL . $domain . GEO_FILTER_DOT_SYMBOL . $tld;
  $realstring = $name . '@' . $domain . '.' . $tld;
  $cid = db_result(db_query("SELECT cid FROM {contact} WHERE category = '%s'", array($hidestring)));
  if (!$cid) {
    drupal_goto('contact', '', NULL, 301);
  }
  drupal_goto('contact', 'edit[cid]='.$cid, NULL, 301);
}

/**
 * Implementation of hook_filter_tips().
 */
function geo_filter_filter_tips($delta, $format, $long = FALSE) {
  switch ($delta) {
    case 0:
      return t('Email addresses and mailto: links will be obfuscated to hide them from spambots but still be readable to javascript-enabled browsers.');
      break;
  }
}


/**
 * Implementation of hook_filter()
 *
 * Replaces email address text with name[at]domain[dot]tld
 * Replaces mailto: links with contact form redirect
 *
 * @param string $op
 * @param int $delta
 * @param int $format
 * @param string $text The text passed to the filter
 * @return string 
 */
function geo_filter_filter($op, $delta= 0, $format= -1, $text= '') {
  switch ($op) {
    case 'list':
      return array (
        0 => t('Email link obfuscator'),
      );
    case 'description':
      return t('Hides email addresses and mailto: links from spam bots.');
    case 'prepare':
      return $text;
    case 'process':
      return geo_filter_obfuscate($text, $format);
    case 'settings':
      return _geo_filter_filter_settings($format);
  }
}

/**
 * Settings form for the geo_filter_filter() filter
 *
 * @param int $format
 * @return array
 */
function _geo_filter_filter_settings($format) {
  $form['geo_filter_filter'] = array(
    '#type' => 'fieldset',
    '#title' => t('Email link obfuscator filter'),
    '#collapsible' => TRUE,
  );
  $form['geo_filter_filter']["geo_filter_show_not_exists_$format"] = array(
    '#type' => 'checkbox',
    '#title' => t('Display notification on new email'),
    '#default_value' => variable_get("geo_filter_show_not_exists_$format", FALSE),
    '#description' => t('When this is selected, the user will be notified each time they create a mailto: link to an email address that has not been added to the contact form.'),
  );
  return $form;
}

/**
 * Performs text processing
 *
 * Uses regex to perform replacement detailed in geo_filter_filter()
 * 
 * @param string $text The text to manipulate
 * @param int $format The input format being used
 * @return string 
 * 
 * @see geo_filter_filter()
 * @todo make strings translatable (currently obfuscation reversal is dependant on strings being static)
 */
function geo_filter_obfuscate($text, $format) {

  /**
   * Replace callback for mailto link regex
   *
   * Using lamda function because it depends on the $format variable which cannot be passed via preg_replace_callback
   * 
   * @param array $matches the matched elements from the regex search
   * @return string 
   */
  $geo_filter_replace_callback = create_function('$matches',
    '$hidestring = $matches[1] . GEO_FILTER_AT_SYMBOL . $matches[2] . GEO_FILTER_DOT_SYMBOL . $matches[3]; '
    . '$realstring = $matches[1] . "@" . $matches[2] . "." . $matches[3]; '
    . '$cid = db_result(db_query("SELECT cid FROM {contact} WHERE category = \'%s\'", array($hidestring))); '
    . 'if (!$cid && variable_get("geo_filter_show_not_exists_' . $format . '", false) && user_access("administer site-wide contact form")) { '
    . 'drupal_set_message(t("!email does not exist in the contact form. In order to best support non-javascript browsers you should add it !link", array("!email" => $realstring, "!link" => "<a href=\"" . url("admin/build/contact/add", array("query" => "edit[category]=" . $hidestring . "&edit[recipients]=" . $realstring)) . "\">here</a>")), "warning"); '
    . '} '
    . 'return "/contact/" . $matches[1] . "/" . $matches[2] . "/" . $matches[3]; ' 
  );

  $text = preg_replace_callback("/\"mailto:([A-Za-z0-9._%-]+)\@([A-Za-z0-9._%-]+)\.([A-Za z]{2,4})\"/i", $geo_filter_replace_callback, $text);
  $text = preg_replace("/([A-Za-z0-9._%-]+)\@([A-Za-z0-9._%-]+)\.([A-Za z]{2,4})/i", "$1" . GEO_FILTER_AT_SYMBOL . "$2" . GEO_FILTER_DOT_SYMBOL . "$3", $text);
  return $text;
}
