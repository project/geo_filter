
Drupal.behaviors.geo_filter = function(context) {
  $("a[href]").each(function(i) {
    var href = $(this).attr("href");
    var address = href.replace(/.*contact\/([a-z0-9._%-]+)\/([a-z0-9._%-]+)\/([a-z.]+)/i,'$1' + '@' + '$2' + '.' + '$3');
    if (href != address)
    {
      $(this).attr("processed", 'processed');
      $(this).attr("href", 'mailto:' + address);
      $(this).html($(this).html().replace(/([a-z0-9._%-]+)\[at\]([a-z0-9._%-]+)\[dot\]([a-z.]+)/i,'$1' + '@' + '$2' + '.' + '$3'));
    }
  });
  $("div:contains('[at]')").each(function(i) {
    $(this).html($(this).html().replace(/([a-z0-9._%-]+)\[at\]([a-z0-9._%-]+)\[dot\]([a-z.]+)/ig,'$1' + '@' + '$2' + '.' + '$3')); 
  });
}
